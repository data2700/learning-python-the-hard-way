# Thank you Mike VanQuickenborne for my college logic course.

# Memorizing logic

# and
# or
# not
# != not equal
# == equal
# >= greater than equal
# <= less than equal
# True
# False

# If something is:

# NOT:
# Not False (then it's True)
# Not True (then it's False)

# OR
# True or False (True)
# True or True (True)
# False or True (True)
# False or False (False)

# AND
# True and False (then it's False)
# True and True (it's True)
# False and True (False)
# False and False (False) <-- No double negatives here!

# NOT OR
# Not (True or False)? It's False
# Not (True or True)? False
# Not (False or True)? False
# Not (False or False)? True <-- There we go!

# NOT AND
# Not (True and False)? It's True
# Not (True and True)? False
# Not (False and True)? True
# Not (False and False)? True

# !=
# 1 != 0: True
# 1 != 1: False
# 0 != 1: True
# 0 != 0: False

# ==
# 1 == 0: False
# 1 == 1: True
# 0 == 1: False
# 0 == 0: True
