people = 20
cats = 30
dogs = 15

if people < cats:
    print "Too many cats!"

if people > cats:
    print "Needs more cats!" # Let's be honest

if people < dogs:
    print "Are you a dog walker?"

if people > dogs:
    print "What a sad place."

dogs += 5

if people >= dogs:
    print "The number of people is greater than or equal to that of dogs. We need more dogs."

if people <= dogs:
    print "The number of people is less than or equal to that of dogs. Go adopt a dog!"

if people == dogs:
    print "People are dogs, and this exercise is messed up."


if people & dogs < 30: # using "and"
    print "The number of people and dogs is less than 30."

# Study drill
# What do you think the if does to the code under it?
# The if statement evaluates whether its given expression(s) is True, and (if it is true) will run the code under it. If there are no True expressions, then it'll run an else statement (if there is one).
# TLDR: if True do X, else do Y.

# Why does the code under the if need to be indented four spaces?
# Because that's how Python determines the scope of the statement.

# What happens if it isn't indented?
# It'll throw an error.

# Can you put other boolean expressions from Exercise 27 in the if- statement?
# Yes.

# What happens if you change the initial variables for people, cats, and dogs?
# The if statements will evaluate those changed values.


# Reading
# Flow control tools: https://docs.python.org/2/tutorial/controlflow.html
# The if statement: https://docs.python.org/2/reference/compound_stmts.html#if
