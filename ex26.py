def break_words(stuff):
    """This function will break up words for us."""
    words = stuff.split(' ')
    return words

def sort_words(words):
    """Sorts the words."""
    return sorted(words)

def print_first_word(words): # Added colon.
    """Prints the first word after popping it off."""
    word = words.pop(0) # Changed to pop.
    print word

def print_last_word(words):
    """Prints the last word after popping it off."""
    word = words.pop(-1) # Added right paren ).
    print word

def sort_sentence(sentence):
    """Takes in a full sentence and returns the sorted words."""
    words = break_words(sentence)
    return sort_words(words)

def print_first_and_last(sentence):
    """Prints the first and last words of the sentence."""
    # words = break_words(sentence)
    # This shouldn't be there, as we aren't splitting this input.
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """Sorts the words then prints the first and last one."""
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)


print "Let's practice everything."
print "You\'d need to know \'bout escapes with \\ that do \n newlines and \t tabs."

poem = """
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explantion
\n\t\twhere there is none.
"""


print "--------------"
print poem
print "--------------"

five = 10 - 2 + 3 - 6 # Changed 5 to 6.
print "This should be five: %d" % five # Changed %s to %d.

def secret_formula(started):
    jelly_beans = started * 500
    jars = jelly_beans / 1000 # Changed forward slash to back slash.
    crates = jars / 100
    return jelly_beans, jars, crates

start_point = 10000
beans, jars, crates = secret_formula(start_point) # Removed one equals sign.

print "With a starting point of: %d" % start_point
print "We'd have %d beans, %d jars, and %d crates." % (beans, jars, crates) # Changed "jeans" to "beans" in the string.

start_point = start_point / 10

print "We can also do that this way:"
print "We'd have %d beans, %d jars, and %d crates." % secret_formula(start_point) # Changed "crabapples" to crates in the string, added right paren ) to argument.

# None of these are necessary

# sentence = "All good things come to those who wait."
# Corrected phrase to "All good things come to those who wait."
# This would be run in the command line and passed through to the functions.

# words = ex26.break_words(sentence)
# changed 25 to 26, in "ex26".
# This would be run in the command line, not here.

# sorted_words = ex26.sort_words(words)
# changed 25 to 26, in "ex26".
# This would be run in the command line, not here.

# print_first_word(words)
# print_last_word(words)

# print_first_word(sorted_words)
# removed period from in front of "print"

# print_last_word(sorted_words)

# sorted_words = ex26.sort_sentence(sentence)
# changed 25 to 26, in "ex26".
# This line would be run in the command line, not here.

# print sorted_words # corrected spelling of "print"

# print_first_and_last(sentence) # corrected spelling of "first"

# print_first_a_last_sorted(senence) # removed indent, corrected spelling of argument to "sentence"

# Note: I did have to reference the ex25-command-line-comments.py file in order to run this module in the command line. I plan to go over this lesson further to better remember how to call these functions.
