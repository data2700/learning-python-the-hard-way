from sys import argv
# From module sys import argv.

script, filename = argv
# Shows what arguments are contained in argv.

txt = open(filename)
# opens the file ex15_sample.txt (the value we'll pass along argv).

print "Here's your file %r." % filename
print txt.read()
# Reads and prints the file.

print "Type the filename again:"
file_again = raw_input("> ")
# Prints the request to input the filename again and prompts the user.

txt_again = open(file_again)
# Opens the file.

print txt_again.read()
# Reads and prints the file.

# See built-in functions for open() https://docs.python.org/2/library/functions.html
