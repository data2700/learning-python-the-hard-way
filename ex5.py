name = 'Zed A. Shaw'
age = 35
height = 74
weight = 180
eyes = 'blue'
teeth = 'white'
hair = 'brown'

print "Let's talk about %s." % name
print "He's %d inches tall." % height
print "He's %d pounds heavy." % weight
print "Actually, that's not too heavy."
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee." % teeth

print "If I add %d, %d, and %d, I get %d." % (age, height, weight, age + height + weight)

# %s is called a formatter (as is %r and %d), it tells Python to take the variable on the right and replace the %s with its value.

# Study drill (turn lbs to kg)
number_of_lbs = 4
lb = 2.2
kg = 1
kg_to_lb = kg / lb

equation = number_of_lbs * kg_to_lb

print round(equation, 2)
# Rounds the result to two decimal places

# Questions
# What are format characters (formatters)?
# How do I round a floating point number?

# Additional reading
# Format characters https://docs.python.org/2.4/lib/typesseq-strings.html
# Built in functions https://docs.python.org/2/library/functions.html
