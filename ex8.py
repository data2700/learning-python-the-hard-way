formatter = "%r %r %r %r"

print formatter % (1,2,3,4)
print formatter % ("one", "two", "three", "four")
print formatter % (True, False, False, True)
print formatter % (formatter, formatter, formatter, formatter)
print formatter % (
    "I had this thing",
    "That you could type up right",
    "But it didn't sing", # Uses double quotes because there's an apostrophe in the string
    "So I said goodnight"
)

# Q&A
# Why did this exercise print single quotes?
# Because it's not printing the value literally, as it's getting passed through a formatter (%r).
