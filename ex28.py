# True and True: True
# False and True: False
# 1 == 1 and 2 == 1: False
# "test" == "test": True
# 1 == 1 and 2 !=1: True
# True and 1 == 1: True
# False and 0 != 0: False
# True or 1 == 1: True
# "test" == "testing": False
# 1 != 0 and 2 == 1: False
# "test" != "testing": True
# "test" == 1: False
# not (True and False): True
# not (1 == 1 and 0!= 1): False
# not (10 == 1 or 3 == 4): True
# not (1 != 10 or 3 == 4): False
# not ("testing" == "testing" and "Zed" == "A cool guy"): True
# 1 == 1 and not ("testing" == 1 or 1 == 0 ): True
# "chunky" == "bacon" and not (3 == 4 or 3 == 3): False
# 3 == 3 and not ("testing" == "testing" or "Python" == "fun"): False

# Extra
# 3 != 4 and not ("testing" != "test" or "Python" == "Python"): False

# Reading
# Comparisons: https://docs.python.org/2.3/ref/comparisons.html
