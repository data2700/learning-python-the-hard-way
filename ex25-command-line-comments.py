$ python
Python 2.7.10 (default, Jul 15 2017, 17:16:57)
[GCC 4.2.1 Compatible Apple LLVM 9.0.0 (clang-900.0.31)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import ex25 # Imports module!
>>> sentence = "All good things come to those who wait." # Assigns value to "sentence".
>>> words = ex25.break_words(sentence) # Separates the content of "sentence" into a list and assigns that value to "words".
>>> words # Because I'm on the command line it's taken as "print".
['All', 'good', 'things', 'come', 'to', 'those', 'who', 'wait.']
>>> sorted_words = ex25.sort_words(words) # Assigns value of "sort_words" to "sorted_words".
>>> sorted_words # Prints "sorted_words"
['All', 'come', 'good', 'things', 'those', 'to', 'wait.', 'who']
>>> ex25.print_first_word(words) # Prints and removes the first word of the list "words".
All
>>> ex25.print_last_word(words) # Prints and removes the last word of the list "words".
wait.
>>> words # Prints "words" (excluding "All" and "wait", as they've been removed).
['good', 'things', 'come', 'to', 'those', 'who']
>>> ex25.print_first_word(sorted_words) # Prints and removes the first word of the list "sorted_words".
All
>>> ex25.print_last_word(sorted_words) # Prints and removes the last word of the list "sorted_words".
who
>>> sorted_words # Prints "sorted_words".
['come', 'good', 'things', 'those', 'to', 'wait.']
>>> sorted_words = ex25.sort_sentence(sentence) # Sorts "sentence" and assigns value to "sorted_words".
>>> sorted_words # Prints "sorted_words"
['All', 'come', 'good', 'things', 'those', 'to', 'wait.', 'who']
>>> ex25.print_first_and_last_sorted(sentence) # Prints first and last words.
All
who
>>>
