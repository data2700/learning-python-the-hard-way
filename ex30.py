people = 30
cars = 40
buses = 15

if cars > people:
    print "We should take the cars."
elif cars < people: # If cars !> people, but cars < people
    print "We should not take the cars."
else: # If cars !> people and cars !< people
    print "We can't decide."

if buses > cars:
    print "That's too many buses."
elif buses < cars: # If buses !> cars, but buses < cars
    print "Maybe we could take the buses."
else: # If buses !> cars and buses !< cars
    print "We still can't decide."

if people > buses:
    print "Alright, let's just take the buses."
else: # If people !> buses
    print "Fine, let's stay home then."

# I'm not a fan of Python's elif syntax.
# It seems like it'd be just as easy to write "else if", without the potential confusion.

# Questions
# In an if statement, if multiple expressions are True, it will default to the first True instance.
# In this case, it'd be best to separate the possibility of multiple True expressions across multiple if statements.
# i.e. "if A is True, then B", and "if one is True, then two" are two separate if statements.
# How do you combine the results of multiple if statements? Assign them to their own respective variables and pass them through another function?

# Reading
# Control flows https://docs.python.org/2/tutorial/controlflow.html
